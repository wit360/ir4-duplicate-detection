from os     import listdir
from random import randint
from heapq  import nlargest
from sys    import argv,stdout

DEFAULT_PRIME = 15487469

class Trigrams:
    def __init__(self):
        self.trigrams = {}

    def label(self,trigram):
        if trigram not in self.trigrams:
            label = len(self.trigrams)
            self.trigrams[trigram] = label
            return label
        else:
            return self.trigrams[trigram]
        
class Document:
    def __init__(self,filename):
        self.filename = filename
        self.trigrams = set()
        self.sketches = dict() # key=sketch number
        
    def generate_sketches(self,hasher):
        for i in range(25):    # 25 sketches
            self.sketches[i] = min( [hasher.hash(i,t) for t in self.trigrams] )

class Hasher: # 25 hash functions for 25 permutations given a large prime number
    def __init__(self,p): # p is the selected large prime number
        self.__p = p
        self.__a = dict()
        self.__b = dict()
        # generate random a,b for h(x) = ax+b mod p
        for i in range(25):
            self.__a[i] = randint(1,p-1)
            self.__b[i] = randint(0,p-1)
    
    def hash(self,i,x):         # h(x) = ax+b mod p
        return (self.__a[i]*x + self.__b[i]) % self.__p

def jaccard_estimate(doc1,doc2):  # given two Document objects, use sketches to estimate J(doc1,doc2)
    return sum([1 if doc1.sketches[i] == doc2.sketches[i] else 0 for i in range(25)])/25.0

"""
def jaccard_full(doc1,doc2):      # given two Document objects, calculate real J(doc1,doc2)
    d1,d2 = doc1.trigrams, doc2.trigrams
    return float(len(d1.intersection(d2)))/len(d1.union(d2))
"""

def main():
    prime    = int(argv[1]) if len(argv) == 2 else DEFAULT_PRIME  # default big prime number
    hasher   = Hasher(prime)                          # hash function for random permutation
    trigrams = Trigrams()                             # manage and label trigrams
    docs     = dict()                                 # set of documents in corpus
    files    = [filename for filename in listdir("test/")]
    n        = len(files)
    
    # read each document in corpus
    stdout.write("Reading:")
    for doc_no,filename in enumerate(sorted(files)):
        if doc_no%10==0: print
        stdout.write(" %2d" % doc_no)
        doc = Document(filename)                    # create Document object
        docs[doc_no] = doc
        
        tokens = list()
        with open("test/" + filename,'r') as file:  # read tokens
            tokens = file.read().split()
        
        for i in range(len(tokens)-2):              # label each trigram and add it to the doc
            doc.trigrams.add( trigrams.label((tokens[i],tokens[i+1],tokens[i+2])) )
        
        doc.generate_sketches(hasher)   # create sketches given 25 permutations (hash functions)
    
    # calculate full jaccard for those estimated jaccard > 0.5
    #similar_pairs = [(jaccard_full(docs[i],docs[j]),jaccard_estimate(docs[i],docs[j]),docs[i],docs[j])
    #                  for i in range(n) for j in range(i,n) 
    #                  if i!=j and jaccard_estimate(docs[i],docs[j]) > 0.5]
    similar_pairs = [(jaccard_estimate(docs[i],docs[j]),docs[i],docs[j])
                      for i in range(n) for j in range(i,n) 
                      if i!=j and jaccard_estimate(docs[i],docs[j]) > 0.5]
    
    print "\n\nList of documents with estimated Jaccard(d1,d2) > 0.5"
    print "Est.   Duplicates"
    for e,d1,d2 in sorted(similar_pairs,reverse=True):
        print "%0.2f   %s %s" % (e,d1.filename,d2.filename)
    
    # 3-NN for document file00.txt-file09.txt
    print "\nThree nearest neighbors"
    print "Document     3-NN"
    for i in range(10):
        knn = nlargest(3,[(jaccard_estimate(docs[i],docs[j]),docs[j].filename) 
                          for j in range(n) if i!=j])
        print "%s - %s(%0.2f) %s(%0.2f) %s(%0.2f)" %\
              (docs[i].filename,knn[0][1],knn[0][0],knn[1][1],knn[1][0],knn[2][1],knn[2][0])

if __name__ == '__main__':
    main()
